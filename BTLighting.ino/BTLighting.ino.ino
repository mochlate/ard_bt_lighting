#define BAR_RED 2
#define BAR_GREEN 3
#define BAR_BLUE 4
#define BEA_RED 5
#define BEA_GREEN 6
#define BEA_BLUE 7
#define STA_RED 8
#define STA_GREEN 9
#define STA_BLUE 10

//Runtime Info
int bar_Power = 0;
int bea_Power = 0;
int sta_Power = 0;

int bar_RunType = 0;
int bea_RunType = 0;
int sta_RunType = 0;

String bar_CurrentColor = "000,000,000";
String bea_CurrentColor = "000,000,000";
String sta_CurrentColor = "000,000,000";

int bar_Speed = 10;
int bea_Speed = 10;
int sta_Speed = 10;

int bar_StepSize = 1;
int bea_StepSize = 1;
int sta_StepSize = 1; 

int r = 0;
int g = 0;
int b = 255;

void setup()
{
  Serial.begin(9600);
  
  Serial.println("Booted");
  
  pinMode(BAR_RED, OUTPUT);
  pinMode(BAR_GREEN, OUTPUT);
  pinMode(BAR_BLUE, OUTPUT);
  pinMode(BEA_RED, OUTPUT);
  pinMode(BEA_GREEN, OUTPUT);
  pinMode(BEA_BLUE, OUTPUT);
  pinMode(STA_RED, OUTPUT);
  pinMode(STA_GREEN, OUTPUT);
  pinMode(STA_BLUE, OUTPUT);
}

void loop()
{  
  Fetch_BT();
  
  if(sta_Power == 1)
  {
    
    
    if(sta_RunType == 1)
    {
      strip_Blue();
      patternStuff();
    }
  }
  else {
    strip_Off();
  }
}

void strip_Blue()
{
  analogWrite(BAR_RED, 0);
  analogWrite(BAR_GREEN, 0);
  analogWrite(BAR_BLUE, 255);
  analogWrite(BEA_RED, 0);
  analogWrite(BEA_GREEN, 0);
  analogWrite(BEA_BLUE, 255);
  analogWrite(STA_RED, 0);
  analogWrite(STA_GREEN, 0);
  analogWrite(STA_BLUE, 255);
  
  r = 255;
  g = 0;
  b = 0;
}

void strip_Off()
{
  analogWrite(BAR_RED, 0);
  analogWrite(BAR_GREEN, 0);
  analogWrite(BAR_BLUE, 0);
  analogWrite(BEA_RED, 0);
  analogWrite(BEA_GREEN, 0);
  analogWrite(BEA_BLUE, 0);
  analogWrite(STA_RED, 0);
  analogWrite(STA_GREEN, 0);
  analogWrite(STA_BLUE, 0); 
}

void Fetch_BT()
{
  if(Serial.available())
  {
    String data = "";
    char cur;
    while((cur = Serial.read()) != '|')
    {
      data += cur;
      delay(2); // BT needs to time respond
    }
    
    String operation = data.substring(0, 2);
    data.replace(operation, "");
    operation.replace(":", "");
    processBT(operation.toInt(), data);
  }
}

void processBT(int operation, String data)
{  
  String zone;
  String stat;
  
  switch(operation)
  {
    case 0: // Toggle power
      zone = data.substring(0,1);
      stat = data.substring(2,3);
      
      if(zone == "0")
        bea_Power = stat.toInt();
      if(zone == "1")
        sta_Power = stat.toInt();
      if(zone == "2")
        bar_Power = stat.toInt();
        
      if(sta_Power == 1 && sta_RunType == 0)
        handleRGB(sta_CurrentColor);
        
    break;
    case 1: // Run Type
      zone = data.substring(0,1);
      stat = data.substring(2,3);
      
      if(zone == "0")
        bea_RunType = stat.toInt();
      if(zone == "1")
        sta_RunType = stat.toInt();
      if(zone == "2")
        bar_RunType = stat.toInt();
    break;
    case 2: // Cycle Interval
      zone = data.substring(0,1);
      stat = data.substring(2);
            
      if(zone == "0")
        bea_Speed = stat.toInt();
      if(zone == "1")
        sta_Speed = stat.toInt();
      if(zone == "2")
        bar_Speed = stat.toInt();
    break;
    case 3: // Cycle Interval
      zone = data.substring(0,1);
      stat = data.substring(2);
            
      if(zone == "0")
        bea_StepSize = stat.toInt();
      if(zone == "1")
        sta_StepSize = stat.toInt();
      if(zone == "2")
        bar_StepSize = stat.toInt();
    break;
    case 4: // Solid Color
      stat = data;
      sta_CurrentColor = stat;
      
      if(sta_Power == 1)
        handleRGB(stat);
      
    break;
  }
}

void handleRGB(String stat)
{
  String r = stat.substring(0, stat.indexOf(","));
  String g = stat.substring(stat.indexOf(",")+1, stat.lastIndexOf(","));
  String b = stat.substring(stat.lastIndexOf(",")+1);
  
  setColor(r.toInt(), g.toInt(), b.toInt());
}

void setColor(int r, int g, int b)
{
  analogWrite(BAR_RED, r);
  analogWrite(BAR_GREEN, g);
  analogWrite(BAR_BLUE, b);
  
  analogWrite(STA_RED, r);
  analogWrite(STA_GREEN, g);
  analogWrite(STA_BLUE, b);
  
  analogWrite(BEA_RED, r);
  analogWrite(BEA_GREEN, g);
  analogWrite(BEA_BLUE, b);
}

void patternStuff()
{
  fadeIn(&r, BAR_RED, STA_RED, BEA_RED, &sta_Speed, &sta_StepSize);
  fadeOut(&b, BAR_BLUE, STA_BLUE, BEA_BLUE, &sta_Speed, &sta_StepSize);
  fadeIn(&g, BAR_GREEN, STA_GREEN, BEA_GREEN, &sta_Speed, &sta_StepSize);
  fadeOut(&r, BAR_RED, STA_RED, BEA_RED, &sta_Speed, &sta_StepSize);
  fadeIn(&b, BAR_BLUE, STA_BLUE, BEA_BLUE, &sta_Speed, &sta_StepSize);
  fadeOut(&g, BAR_GREEN, STA_GREEN, BEA_GREEN, &sta_Speed, &sta_StepSize);
}

void fadeIn(int* colorPointer, int pin1, int pin2, int pin3, int* rate, int* stepSize)
{
  for(int i = 0; i < 255; i += *stepSize)
  {
    *colorPointer = i;
    outputColor();
    
    analogWrite(pin1, i);
    analogWrite(pin2, i);
    analogWrite(pin3, i);
    delay(*rate); 
    Fetch_BT();
    if(sta_RunType == 0 || sta_Power == 0)
      break;
  }
}

void fadeOut(int* colorPointer, int pin1, int pin2, int pin3, int* rate, int* stepSize)
{
  for(int i = 255; i > 0; i -= *stepSize)
  {
    *colorPointer = i;
    outputColor();
    
    analogWrite(pin1, i);
    analogWrite(pin2, i);
    analogWrite(pin3, i);
    delay(*rate);
    Fetch_BT();
    if(sta_RunType == 0  || sta_Power == 0)
      break;
  }
}

void outputColor()
{
  String x = String(r) + "," + String(g) + "," + String(b);
  //Serial.println(x);
}
